from tools.codec.factory import CodecFactory

if __name__ == '__main__':
    codec = CodecFactory.new_codec("gzip")
    compressed = codec.compress(b'hello world')
    print(codec.decompress(compressed))