from ._codec import GzipCodec, CodecType, _CODEC_GENERATOR, AutoDecompressor


class CodecFactory(object):
    def __init__(self):
        raise AssertionError()

    @staticmethod
    def new_codec(type_: str):
        codec_type = CodecType.get_codec_type_by_name(type_)
        codec_generator = _CODEC_GENERATOR[codec_type]
        if not codec_generator:
            raise RuntimeError("An unsupported codec type named %s" % type_)
        return codec_generator()

    @staticmethod
    def new_gzip_codec():
        return GzipCodec()

    @staticmethod
    def new_auto_decompressor():
        return AutoDecompressor()


