1. 目录
```text
vulnDBGen(root)
├── config.py : 项目的全局配置
├── initialize.py : 用于项目的第一次运行，主要是将所有CVE检索数据进行下载存储
├── data : 本目录下保存的是相关CVE的信息用于检索，包括CVE出现的时间以及相关内容
├── docs : 本项目的相关手册
├── abs  : 当前主要是存储第三方代码仓库的漏洞函数相关抽象特征，当前包括两部分：一个是漏洞函数特征features，一个漏洞函数的修复属性changes
├── diff : 用于存储第三方代码仓库的漏洞函数在漏洞修复前后的diff信息
├── hidx : 已废弃
├── tmp  : 用于存储中间临时文件，可忽略
├── vul  : 用于存储第三方代码仓库中检索出来的所有漏洞函数代码，其中.patch表示修复信息，.OLD表示未修复时的代码，.NEW表示修复之后的代码
├── src
│   ├── get_cvepatch_from_git.py : 从指定的代码仓库中筛选CVE相关的漏洞补丁信息，数据写到diff目录
│   ├── get_source_from_cvepatch.py : 从已经得到的补丁信息中得到漏洞函数代码以及相关的特征数据，数据写入到vul目录以及abs目录
│   ├── vul_dup_remover.py : 对于不同的第三方代码库，可能存在相同的漏洞函数，所以通过这个脚本进行去重
│   ├── vul_verifier.py : 主要是去除可能存在的漏洞函数误报的情况，基本策略就是若一个漏洞函数未修复时的函数原型与修复后的函数原型相同，则视这个漏洞函数为误报，将其删除
│   └── vul_hidx_generator.py : 已经废弃使用
└── tools
    ├── FuncParser-opt.jar : 一个函数解析器，实现上使用antlr框架，从vul目录下的漏洞函数源码中解析出function，并提取其简单的函数特征。注意需要机器带有java环境
    ├── parseutility.py : 函数解析以及特征提取等，相关的辅助工具。
    └── cvedatagen
        ├── common.py : Data file creation helper library
        ├── cveXmlDownloader.py : CVE information XML file download
        ├── cveXmlParser.py : Parsing XML files and generating data files
        └── cveXmlUpdater.py : Update new/modified CVE information
```

2. 核心模块
```text
$ python get_cvepatch_from_git.py [-h] [-m] REPO_NAME
    - Function: CVE patch extraction from cloned Git repository
    - Input: Specified Git repository
    - Output: CVE patches (*.diff)
    - Arguments: -h: show help message
    -m: run in multimode (for sub-repository, see 5.D)
$ python get_source_from_cvepatch.py [-h] [-m] REPO_NAME
    - Function: Restore old and new functions from the extracted CVE patch
    - Input: CVE patches (*.diff)
    - Output: Old (unpatched) functions (*_OLD.vul)
    New (patched) functions (*_NEW.vul)
    Patch contents (*.patch)
    - Arguments: -h: show help message
    -m: run in multimode (for sub-repository, see 5.D)
$ python vul_dup_remover.py
    - Function: Deduplication of regenerated old functions
    - Input: Saved old/new functions (*.vul)
$ python vul_verifier.py
    - Function: Batch removal of false positive cases
    - Input: Saved old/new functions (*.vul)
```
3. Requirements
    1. Supported operating system
        This solution supports POSIX compatible operating systems (Linux, macOS) and Windows.
    2. Dependent programs by operating system
        1. Windows
            - Python 3.x (https://www.python.org/downloads)
            - Git for Windows (https://git-for-windows.github.io)
        2. macOS
            - Python 3.x, Git, Java need. Can be installed with the following command.
            ```text
            $ brew install python git
            $ brew cask install java
            
            ```
        3. Linux
            - Python 3.x, Git, Java need. Can be installed with the following command.
            ```text
             ex) Debian, Ubuntu, Linux Mint
            $ sudo apt-get install python git openjdk-8-jre
            ```
4. Basic usage
    1. Preferences: config.py

        Before using the solution, you need to configure config.py.
        ```text
        ~/vulnDBGen$ vim config.py
        ```
        1. Common
            - GIT_STORAGE_PATH: The root directory to clone the Git repository to collect vulnerabilities.
            - VERSION: Valid database version when using IoTcube. In general, it is not necessary to set it.
            
            ```text
            GIT_STORAGE_PATH = r"/Users/bytedance/gitRepo"
            
            # 指定版本
            VERSION = "3.1.0"  # for use in IoTcube.
            ```
        2. POSIX
   
            Package manager: If git and java are installed through, the default values below are used.
            ```text
            JAVA_BIN = 'java'
            GIT_BIN = r'git'
            DIFF_BIN = r'diff'
            ```
            Custom build: When using, write the path and binary name for each.
            
        3. Windows

            Enter the exact path and binary name of git.exe and diff.exe installed through Git for Windows (or other installation method).
            
            ```text
            JAVA_BIN = 'java'
            GIT_BIN = r'G:\Git\bin\git.exe'
            DIFF_BIN = r'G:\Git\usr\bin\diff.exe'
            ```

    2. Reset: initialize.py
        1. Common
            
            initialize.py: Creates the required directory, collects CVE information registered in the National Vulnerability Database and 
            saves it in the vulnDBGen_py3/data/cvedata.pkl file.
        
        2. Windows
        
            When initializing in Windows, download FuncParser-opt.exe, a parser for Windows, and save it in vulnDBGen/tools. 
            (POSIX parser is included in the project package.)
            
            对于国内需要翻墙的用户，可以在https://gitee.com/ChiZhung/func-parser/blob/master/FuncParser-opt.zip
            中下载win下的func parser
            
            ```shell script
            $ cd /home/squizz/vulnDBGen
            ~/vulnDBGen$ python initialize.py
            ```
            
            ps: 若第一次执行仅仅执行python initialize.py就ok
            
            The above command sequentially executes cveXmlDownloader.py, cveXmlParser.py, 
            and cveXmlUpdater.py under tools/cvedatagen/. 
            If cvedata.pkl is created through the first execution, 
            then when data (new CVE information, etc.) is updated You only need to run cveXmlUpdater.py alone. 
            In addition, the result is the same even if cveXmlDownloader.py, cveXmlParser.py, and 
            cveXmlUpdater.py are executed sequentially without 
            automatically initializing through initialize.py.
   
    3. Git repository Clone
       
       Git object of target repository is required to extract vulnerability. 
       Clone the Git repository to collect vulnerabilities in the path specified in gitStoragePath of config.py to obtain Git objects. ex) Cloning process of Apache HTTPD and Linux kernel Git repository
       
       ```shell script
        $ cd /home/squizz/vulnDBGen
        ~/vulnDBGen$ python initialize.py
        ~$ cd /home/squizz/gitrepos
        ~/gitrepos$ git clone https://github.com/apache/httpd.git
        Cloning into 'httpd'...
        remote: Counting objects: 457213, done.
        remote: Compressing objects: 100% (65/65), done.
        remote: Total 457213 (delta 34), reused 46 (delta 19), pack-reused 457129
        Receiving objects: 100% (457213/457213), 265.54 MiB | 1.84 MiB/s, done.
        Resolving deltas: 100% (379737/379737), done.
        Checking connectivity... done.
        ~/gitrepos$ git clone https://kernel.googlesource.com/pub/scm/linux/
        kernel/git/torvalds/linux.git
        Cloning into 'linux'...
        remote: Sending approximately 1.04 GiB ...
        remote: Counting objects: 83992, done
        remote: Finding sources: 100% (241/241)
        remote: Total 5403087 (delta 4534718), reused 5402959 (delta 4534718)
        Receiving objects: 100% (5403087/5403087), 1.03 GiB | 9.19 MiB/s, done.
        Resolving deltas: 100% (4534718/4534718), done.
        Checking connectivity... done.
        Checking out files: 100% (59845/59845), done.
        ~/gitrepos$ ls –l
        total 8
        drwxrwxr-x 12 squizz squizz 4096 Jun 21 16:03 httpd
        drwxrwxr-x 23 squizz squizz 4096 Jun 21 16:06 linux 
       ```
       
    4. CVE Patch extraction: get_cvepatch_from_source.py
        * get_cvepatch_from_git.py Extracts the CVE vulnerability patch from 
            the Git repository cloned in the previous step (4.C) and saves it as a *.diff file under vulnDBGen/diff/REPO_NAME/.
        
        ex) CVE patch extraction process from each HTTPD and Linux kernel repository
        
        ```shell script
        ~$ cd /home/squizz/vulnDBGen
        ~/vulnDBGen$ python src/get_cvepatch_from_source.py httpd
        Retrieving CVE patch from httpd
        Multi-repo mode: OFF.
        Initializing... Done.
        205 commits in httpd
        [+] Writing CVE-2014-3583_5.0_CWE-119_bfee66~.diff Done.
        [+] Writing CVE-2015-3183_5.0_~
        …
        154 patches saved in /home/squizz/Desktop/vulnDBGen/diff/httpd
        Done. (1.13679718971 sec)
        ~/vulnDBGen$ python src/get_cvepatch_from_source.py linux
        Retrieving CVE patch from linux
        Multi-repo mode: OFF.
        Initializing... Done.
        311 commits in linux
        [+] Writing CVE-2016-3713_5.6_CWE-284_9842df~.diff Done.
        [+] …
        257 patches saved in /home/squizz/Desktop/vulnDBGen/diff/linux
        Done. (10.4052040577 sec)
        ```
        
    5. Restore function before/after patch: get_source_from_cvepatch.py
        * get_source_from_cvepatch.py Restores the pre-patch and post-patch functions from the collected CVE vulnerability patches, respectively. The restored functions are stored under vulnDBGen/vul/REPO_NAME/. 
        Functions before the vulnerability patch are stored as *_OLD.vul as vulnerable functions, and functions after the patch are stored as *_NEW.vul as safe functions.
    
        ex) Restoring functions before/after patching from CVE patch of HTTPD and Linux kernel respectively
        
        ```shell script
        ~$ cd /home/squizz/vulnDBGen
        ~/vulnDBGen$ python src/get_source_from_cvepatch.py httpd
        Retrieve vulnerable functions from httpd
        Multi-repo mode: Off
        1/154 [+] CVE-2014-0231_5.0_CWE-399_a30c26~.diff (proceed)
        2/154 [+] CVE-2011-4317_4.3_CWE-020_8ab3c7~.diff (proceed)
        …
        Done getting vulnerable functions from httpd
        Reconstructed 178 vulnerable functions from 154 patches.
        Elapsed: 52.60 sec
        ~/vulnDBGen$ python src/get_source_from_cvepatch.py linux
        Retrieve vulnerable functions from linux
        Multi-repo mode: Off
        1/257 [+] CVE-2005-0504_4.6_CWE-119_a2f729~.diff (proceed)
        …
        Done getting vulnerable functions from linux
        Reconstructed 372 vulnerable functions from 257 patches.
        Elapsed: 165.58 sec
        ```
        
    6. Eliminate redundant sources of vulnerability: vul_dup_remover.py
        
        vulnDBGen/vul/: Among all the vulnerable functions stored below, only one duplicate file is left and removed. ex) Deduplication from vulnerable functions in HTTPD and OpenSSL repository
        
        ```shell script
        ~$ cd /home/squizz/vulnDBGen
        ~/vulnDBGen$ python src/vul_dup_remover.py
        [RESULT]
            httpd: deleted 31 duplicate files from 534 files.
            linux: deleted 2 duplicate files from 1101 files.
        Total: 33 duplicate files.
        ```
    7. Batch removal of false positive cases: vul_verifier.py
    
        Cases that cause false positives when detecting code clones are collectively removed from this module. Currently, two removal rules are applied, and rules can be added/removed arbitrarily according to the policy.
        
        rule 1) Deleted if the abstraction result of the old function and the new function are the same
        
        rule 2) Deleted when it is impossible to identify the before/after relationship of the patch due to the change of the function header
        
        ex) Remove false positives from the collection results of vulnerable functions of HTTPD, OpenSSL, and Linux kernel
        
        ```shell script
        ~/vulnDBGen$ python src/vul_verifier.py
        removed 5 FP records from linux
        ```
    8. Generate hash index file of vulnerable source: vul_hidx_generator.py
        
        Upon completion of the preceding process (A~G), only the vulnerable functions from which duplicates and false positive cases were removed remain in vulnDBGen/vul/ or below. vul_hidx_generator.py creates hash indexes of residual functions. The generated hash index has the *.hidx extension and is saved under vulnDBGen/hidx/.
        
        At this time, two types of indexes can be created depending on the purpose:
        
        When -a 0 is used as an argument, index creation for exact matching
        
        When -a 4 is used as an argument, index creation for abstract matching
        
        ```text
        ~/vulnDBGen$ python src/vul_hidx_generator.py –a 0 linux
        loading source (done)
        1 / 360 linux/CVE-2013-1797_6.8_CWE-399_0b7945~_x86.c_106_OLD.vul
        …
        Hash index saved to: ~/vulnDBGen/hidx/hashmark_0_linux.hidx
        Elapsed time: 35.7726960182
        ```
       
        Detailed technical information on hash index creation and exact matching / abstract matching can be found in the paper “VUDDY: A Scalable Approach for Vulnerable Code Clone Discovery” (https://ccs.korea.ac.kr/pds/SNP17.pdf)

5. 如何使用Multimode

    1. what is multimode

        If multiple Git repositories are managed as one project, such as Google Android Project (https://android.googlesource.com/), batch processing is possible using Multimode. ex) Git repositories on Android
        ```text
        accessories/manifest
        api_council_filter
        brillo/manifest
        cts_drno_filter
        device/aaeon/upboard
        device/asus/deb
        ```
    2. Multiple Git repository Clone
        
        config.py: After creating a directory with the project name (e.g., android) under the gitStoragePath specified in, clone all the repositories.
        
        ex) All Git sub-repository cloning process of Android project
        ```text
        ~$ cd /home/squizz/gitrepos (directory specified in config.py)
        ~/gitrepos$ mkdir android && cd android
        ~/android$ git clone https://android.googlesource.com/accessories/manifest
        accesories/manifest
        ~/android$ git clone https://android.googlesource.com/brillo/manifest
        brillo/manifest
        (Omit output)
        ```
    3. Create List File
        
        The list file to store the sub-repository list of the project is saved in the list_project name file under vulnDBGen/data/repolists/.
        
        ex) Generating Android project's Git repository list file
        ```shell script
        ~$ cd /home/squizz/vulnDBGen/data
        ~/data$ mkdir repolists && cd repolists
        ~/repolists$ touch list_android
        ```
        
        ex) Save the list of sub-repository in the created list_android file.
        ```shell script
        ~/repolists$ cat list_android
        accessories/manifest
        api_council_filte
        ...
        ```
    
    4. CVE patch extraction and pre/post patch function restoration process
    
        get_cvepatch_from_git.py: The execution method of get_source_from_cvepatch.py ​​and get_source_from_cvepatch.py ​​are the same, but multimode is activated by using an additional -m argument.
        
        ex) Extract CVE patch from all Git repository of Android project and restore function
        
        ```shell script
        ~$ cd /home/squizz/vulnDBGen
        ~/vulnDBGen$ python src/get_cvepatch_from_source.py –m android
        Retrieving CVE patch from httpd
        Multi-repo mode: ON.
        Initializing... Done.
        …
        ~/vulnDBGen$ python src/get_source_from_cvepatch.py –m android
        Retrieve vulnerable functions from linux
        Multi-repo mode: On
        …
        ```
    5. post process
        
        The subsequent process is the same as described in 4.E ~ 4.H.
6. backup
    1. inquiry
        
        If a problem occurs while using the solution, error! The reference source could not be found. Please contact us.
    
    2. Contact information
        
        CSSA
        Phone: (+82)2-3290-4816
        E-mail: cssa@korea.ac.kr
        Web: https://cssa.korea.ac.kr
        