#!/usr/bin/env python
"""
CVE data updater.
Run cveXmlDownloader.py and cveXmlParser.py before running this module.
This module downloads "modified" data from NVD, uncompress and update the database.
"""

import os
import pickle
from . import common
import config
import logging


def process():
    # first download the modified cve data from NVD
    fileName = "nvdcve-1.1-modified.json.zip"
    url = "https://nvd.nist.gov/feeds/json/cve/1.1/" + fileName

    common.download_url(url, fileName)
    common.unzip(fileName)
    os.remove(fileName)

    # load the pickled cve data
    logging.info("Reading pickled data...",)
    cveDict = pickle.load(open(os.path.join(config.ROOT_PATH, "data", "cvedata.pkl"), "rb"))
    logging.info("[DONE]")

    subDict = common.parse_xml(fileName.replace(".zip", ""))
    cveDict.update(subDict)

    os.remove(fileName.replace(".zip", ""))

    logging.info("Dumping updated pickle...",)
    pickle.dump(cveDict, open(os.path.join(config.ROOT_PATH, "data", "cvedata.pkl"), "wb"))
    logging.info("[DONE]")


if __name__ == '__main__':
    config.conf_log()
    process()
