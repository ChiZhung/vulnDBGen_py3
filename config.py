import os
import platform
import sys

ROOT_PATH = root_path = os.path.abspath(os.path.dirname(__file__))

# 指定通过git下载的repo存储的路径

GIT_STORAGE_PATH = r"/Users/bytedance/gitRepo"

# 指定版本
VERSION = "3.1.0"  # for use in IoTcube.

DEBUG_MODE = False

# log
LOG_STREAM = sys.stderr
LOG_LEVEL = 'INFO' if not DEBUG_MODE else 'DEBUG'
LOG_FMT = '%(asctime)s [%(levelname)s] %(message)s'
LOG_DATE_FMT = '%Y-%m-%d %H:%M:%S'

ENCODING = 'utf-8'

VALID_SOURCE_SUFFICES = [
    ".c", ".cpp", ".cc", ".c++", ".cxx", ".h", ".hpp"
]

# 4MB
MAX_FILE_SIZE = 1024 * 1024 * 4

OS_PLATFORM = platform.platform()
OS_WINDOWS = 'Windows'
OS_LINUX = 'Linux'
OS_OSX = 'Darwin'


def __os(os_type: str) -> bool:
    return os_type in OS_PLATFORM


def os_is_windows():
    return __os(OS_WINDOWS)


def os_is_linux():
    return __os(OS_LINUX)


def os_is_osx():
    return __os(OS_OSX)


if os_is_windows():
    JAVA_BIN = 'java'
    GIT_BIN = r'G:\Git\bin\git.exe'
    DIFF_BIN = r'G:\Git\usr\bin\diff.exe'
elif os_is_linux():
    JAVA_BIN = 'java'
    GIT_BIN = r'git'
    DIFF_BIN = r'diff'
elif os_is_osx():
    JAVA_BIN = 'java'
    GIT_BIN = r'git'
    DIFF_BIN = r'diff'
else:
    JAVA_BIN = 'java'
    GIT_BIN = r'git'
    DIFF_BIN = r'diff'

CVE_KEYWORD = 'CVE-20'


def conf_log():
    import logging
    logging.basicConfig(stream=LOG_STREAM, level=LOG_LEVEL, datefmt=LOG_DATE_FMT, format=LOG_FMT)
