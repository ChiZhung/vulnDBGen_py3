#!/usr/bin/env python
"""
NVD's CVE xml data processor.
xml data is downloaded from https://nvd.nist.gov/download.cfm
This module should be run only once.
or, if the pickle file has been corrupted, run this module again.
Updates of the database is done in cvexmlupdater.py
"""

import os
from . import common
import config
import logging

try:
    import cPickle as pickle
except ImportError:
    import pickle


def process():
    DLDir = os.path.join(config.ROOT_PATH, "data", "CVEXML")
    cveDict = {}

    for xml in os.listdir(DLDir):
        subDict = common.parse_xml(os.path.join(DLDir, xml))
        cveDict.update(subDict)

    pickle.dump(cveDict, open(os.path.join(config.ROOT_PATH, "data", "cvedata.pkl"), "wb"))

    logging.info("Stored " + str(len(cveDict)) + " CVE data in file 'cvedata.pkl'.")


if __name__ == '__main__':
    config.conf_log()
    process()
